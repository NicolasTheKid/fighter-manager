import { readFileSync } from 'fs';
import Level from './Level';

export const loadMap = (level: Level) => {
  return readFileSync(level.map).toString();
};
