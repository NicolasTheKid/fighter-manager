import { Observer } from './observer';

export interface Observable {
  registerObserver(name: string, Observer: Observer): void;
  removeObserver(name: string): void;
  notify<T>(message: {type: any, data: any}, name: string): void;
}