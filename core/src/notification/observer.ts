export interface Observer {
  receiveNotification<T>(message: {type: any, data: any}): void;
}