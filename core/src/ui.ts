import * as blessed from 'blessed';
// import MapComponent from '../ui/components/map.component';

export default class GameUI {
  private baseFormConfig: blessed.Widgets.FormOptions;
  private components: blessed.Widgets.Node[] = [];

  private gameScreen: blessed.Widgets.Screen;
  private gameForm: blessed.Widgets.FormElement<unknown>;

  /**
   *
   */
  constructor() {
    this.baseFormConfig = {
      label: 'Game Engine Testing',
      keys: true,
      left: 0,
      top: 0,
      height: '80%',
      width: '80%',
      bg: '#3d3d3d',
      border: {
          type: 'line'
      }
    };
    this.Init();
  }

  /**
   * Init
   */
  private Init() {
    this.gameScreen = blessed.screen({
      smartCSR: true
    });

    this.gameScreen.key(['escape', 'q', 'C-c'], () => process.exit(0));

    this.gameForm = blessed.form({
      parent: this.gameScreen,
      ...this.baseFormConfig
    });

    this.gameScreen.render();
  }

  /**
   * AddComponent
   * adds a component to the UI
   */
  public AddComponent(component: blessed.Widgets.Node) {
    this.gameForm.append(component);
    this.components.push(component);
  }

  /**
   * Draw
   * updates the UI
   */
  public Draw() {
    this.gameScreen.render();
  }
}
