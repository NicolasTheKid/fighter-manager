export default interface GameOptions {
  framerate: number;
  tick_length: number;
  tick_length_ms?: number;
}