import { Random } from '../random';

export default function statGeneration(
  race: {
    minHeight: number,
    maxHeight: number,
    minWeight: number,
    maxWeight: number,
    statIncrease: any,
  },
  characterClass: { preferedStats: any }): any {

  const stats: any = {};
  stats.core = {};
  stats.melee = {};
  stats.ranged = {};
  stats.spell = {};
  stats.healing = {};
  stats.defense = {};
  stats.utility = {};
  const random = new Random();
  stats.core.con = random.nextInt32([1, 10]);
  stats.core.str = random.nextInt32([1, 10]);
  stats.core.dex = random.nextInt32([1, 10]);
  stats.core.wis = random.nextInt32([1, 10]);
  stats.core.int = random.nextInt32([1, 10]);
  stats.core.cha = random.nextInt32([1, 10]);
  stats.core.luck = random.nextInt32([1, 11]);
  stats.height = random.nextInt32([race.minHeight, race.maxHeight]);
  stats.weightRatio = random.nextInt32([race.minWeight, race.maxWeight]).toFixed(2);
  stats.weight = Math.floor(+stats.weightRatio * stats.height + (stats.core.con * 0.25) + (stats.core.str * 0.75));
  /* Melee */
  stats.melee.power = Math.floor((stats.core.str * 0.75) + (stats.core.dex * 0.25));
  stats.melee.hitChance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  stats.melee.critChance = Math.floor((stats.core.str * 0.25) + (stats.core.dex * 0.75));
  stats.melee.attackSpeed = (stats.core.str * 1.0);
  stats.melee.armorPenetration = Math.floor((stats.core.str * 0.65) + (stats.core.dex * 0.35));
  stats.melee.disarmChance = Math.floor((stats.core.str * 0.35) + (stats.core.dex * 0.65));
  /* Ranged */
  stats.ranged.power = Math.floor((stats.core.str * 0.25) + (stats.core.dex * 0.75));
  stats.ranged.hitChance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  stats.ranged.critChance = Math.floor((stats.core.str * 0.25) + (stats.core.dex * 0.75));
  stats.ranged.attackSpeed = Math.floor((stats.core.str * 0.30) + (stats.core.dex * 0.70));
  stats.ranged.armorPenetration = Math.floor((stats.core.str * 0.65) + (stats.core.dex * 0.35));
  /* Spell */
  stats.spell.power = (stats.core.int * 1.0);
  stats.spell.hitChance = Math.floor((stats.core.wis * 0.35) + (stats.core.int * 0.65));
  stats.spell.critChance = Math.floor((stats.core.int * 0.70) + (stats.core.wis * 0.30));
  stats.spell.castingSpeed = Math.floor((stats.core.wis * 0.50) + (stats.core.int * 0.50));
  stats.spell.resistancePenetration = (stats.core.wis * 1.0);
  /* Healing */
  stats.healing.power = Math.floor((stats.core.wis * 0.60) + (stats.core.int * 0.40));
  /* Defense */
  stats.defense.blockValue = (stats.core.str * 1.0);
  stats.defense.blockChance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  stats.defense.dodge = (stats.core.dex * 1.0);
  stats.defense.parry = Math.floor((stats.core.str * 0.70) + (stats.core.dex * 0.30));
  stats.defense.disarmResistance = Math.floor((stats.core.str * 0.75) + (stats.core.wis * 0.25));
  stats.defense.physicalResistance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  // stats.defense.natureResistance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  // stats.defense.arcaneResistance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  // stats.defense.frostResistance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  // stats.defense.fireResistance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  // stats.defense.holyResistance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  // stats.defense.shadowResistance = Math.floor((stats.core.wis * 0.35) + (stats.core.dex * 0.65));
  /* Utility */
  stats.utility.willpower = (stats.core.wis * 1.0);
  stats.utility.endurance = Math.floor((stats.core.con * 0.75) + (stats.core.str * 0.25));
  stats.utility.spirit = (stats.core.wis * 1.0);
  stats.utility.sneak = Math.floor(9 - (4.5 * (stats.weight / 250)) - (4.5 * (stats.height / 74)) + (stats.core.dex * 0.25));
  stats.utility.lockPicking = Math.floor((stats.core.wis * 0.70) + (stats.core.dex * 0.30));
  stats.utility.speech = (stats.core.cha * 1.0);
  stats.utility.intimidation = Math.floor(((stats.core.str * 0.55) + (stats.core.cha * 0.45)) + (2 * (stats.weight / 250)) + (2 * (stats.height / 74)));
  stats.utility.perception = Math.floor((stats.core.wis * 0.70) + (stats.core.con * 0.30));
  stats.utility.faith = (stats.core.cha * 1.0);

  return stats;
}
