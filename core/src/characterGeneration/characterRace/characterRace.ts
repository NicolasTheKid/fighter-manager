import * as path from 'path';
import { Random } from '../../random';

export default function randomRace() {
  const races = require(path.normalize(`${__dirname}/characterRaces.json`)).races;
  const random = new Random();

  const raceIndex = random.nextInt32([0, races.length]);
  return races[raceIndex];
}