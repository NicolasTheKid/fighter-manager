import * as path from 'path';
import { Random } from '../../random';

export default function randomClass() {
  const classes = require(path.normalize(`${__dirname}/characterClasses.json`)).classes;
  const random = new Random();

  const classIndex = random.nextInt32([0, classes.length]);
  return classes[classIndex];
}