import CharacterClass from './characterClass/characterClass';
import CharacterRace from './characterRace/characterRace';
import StatGenerator from './statGenerator';
import Character from './character';
import CombatNotificationCenter from '../battle/combat/combatNotificationCenter';

export default function characterGenerator(race: any, characterClass: any, stats: any, name: string, combatNotificationCenter: CombatNotificationCenter): any {
  const inventory: any[] = [];
  const charClass = CharacterClass();
  const charRace = CharacterRace();
  const charStats = StatGenerator({
    minHeight: charRace.minHeight,
    maxHeight: charRace.maxHeight,
    minWeight: charRace.minWeight,
    maxWeight: charRace.maxWeight,
    statIncrease: charRace.statIncrease
  }, {
    preferedStats: charClass.preferedStats
  });
  return new Character(charStats, charRace.type, charClass.type, name, combatNotificationCenter);
}