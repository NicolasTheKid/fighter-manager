import CombatNotificationCenter from '../battle/combat/combatNotificationCenter';
import CombatNotificationClient from '../battle/combat/combatNotificationClient';
import {Observer} from '../notification/observer';

export default class Character {
  private target: string;
  private combatNotificationClient: CombatNotificationClient;
  private health: number;

  constructor(
    private stats: {
      height: number,
      weight: number,
      weightRatio: number,
      visionRange: 2500,
      core: {
        con: number,
        str: number,
        dex: number,
        int: number,
        wis: number,
        cha: number,
        luck: number
      }, melee: {
        power: number,
        hitChance: number,
        critChance: number,
        attackSpeed: number,
        armorPenetration: number,
        disarmChance: number
      }, ranged: {
        power: number,
        hitChance: number,
        critChance: number,
        attackSpeed: number,
        armorPenetration: number
      }, spell: {
        power: number,
        hitChance: number,
        critChance: number,
        castingSpeed: number,
        resistancePenetration: number
      }, healing: {
        power: number
      }, defense: {
        block: number,
        dodge: number,
        parry: number,
        disarmResistance: number,
        physicalResistance: number
      }, utility: {
        willpower: number,
        endurance: number,
        spirit: number,
        sneak: number,
        lockPicking: number,
        speech: number,
        intimidation: number,
        perception: number,
        faith: number
      }
    },
    private race: string,
    private characterClass: string,
    private name: string,
    private combatNotificationCenter: CombatNotificationCenter) {
    this.health = this.stats.core.con;
    this.combatNotificationClient = new CombatNotificationClient(combatNotificationCenter);
    this.combatNotificationClient.on('message', (message: {type: string, data: any}) => {
      this.consumeMessage(message);
    });
  }

  get Name(): string {
    return this.name;
  }

  get Race(): string {
    return this.race;
  }

  get CharacterClass(): string {
    return this.characterClass;
  }

  get Height(): number {
    return this.stats.height;
  }

  get Weight(): number {
    return this.stats.weight;
  }

  get Health(): number {
    return this.health;
  }

  get CoreStats(): {
    con: number,
    str: number,
    dex: number,
    int: number,
    wis: number,
    cha: number,
    luck: number
  } {
    return this.stats.core;
  }

  get MeleeStats(): {
    power: number,
    hitChance: number,
    critChance: number,
    attackSpeed: number,
    armorPenetration: number,
    disarmChance: number
  } {
    return this.stats.melee;
  }

  get RangedStats(): {
    power: number,
    hitChance: number,
    critChance: number,
    attackSpeed: number,
    armorPenetration: number
  } {
    return this.stats.ranged;
  }

  get SpellStats(): {
    power: number,
    hitChance: number,
    critChance: number,
    castingSpeed: number,
    resistancePenetration: number
  } {
    return this.stats.spell;
  }

  get Healing(): {
    power: number
  } {
    return this.stats.healing;
  }

  get DefenseStats(): {
    block: number,
    dodge: number,
    parry: number,
    disarmResistance: number,
    physicalResistance: number
  } {
    return this.stats.defense;
  }

  get UtilityStas(): {
    willpower: number,
    endurance: number,
    spirit: number,
    sneak: number,
    lockPicking: number,
    speech: number,
    intimidation: number,
    perception: number,
    faith: number
  } {
    return this.stats.utility;
  }

  set Target(target: string) {
    this.target = target;
  }

  get Target(): string {
    return this.target;
  }

  get Observer(): Observer {
    return this.combatNotificationClient;
  }

  attack() {
    this.combatNotificationCenter.notify({type: 'attack', data: this.MeleeStats.power}, this.target);
  }

  private consumeMessage(message: {type: string, data: any}) {
    switch (message.type) {
      case 'attack':
        this.handleAttack(message.data);
        break;
      default:
        break;
    }
  }

  private handleAttack(damage: number) {
    this.health -= damage;
  }
}