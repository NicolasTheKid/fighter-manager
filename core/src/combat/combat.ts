import { Random } from '../../src/random';
import character from '../characterGeneration/character';
import { Dictionary, List } from 'linq-collections';
// import Actions from './actions';
// import NotificationCenter from '../../notification/notificationCenter';
// import NotificationClient from '../../notification/notificationClient';

export class CombatLoop {
    private random: Random;
    // fighter queue
    private actionNotificationQueue: List<{
        action: {};
        origin: string,
        target: string
    }>;
    // innate miss thresholds
    private attackMiss = 20;
    private blockMiss = 20;
    private dodgeChance = 95;
    private parryChance = 90;
    constructor() {
        this.random = new Random();
    }
}
