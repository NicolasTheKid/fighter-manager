import { Observable } from '../../notification/observable';
import { Observer } from '../../notification/observer';

export default class CombatNotificationCenter implements Observable {
    private observers: {name: string, observer: Observer}[];
    constructor() {
        this.observers = [];
    }

    registerObserver(name: string, observer: Observer): void {
        this.observers.push({name: name, observer: observer});
    }

    removeObserver(name: string): void {
        for (let index = 0; index < this.observers.length; index++) {
            if (this.observers[index].name === name) {
                this.observers.splice(index, 1);
            }
        }
    }

    notify<T>(message: {type: string, data: any}, name: string) {
        const combatObserver = this.observers.find(combatObserver => combatObserver.name === name);
        combatObserver.observer.receiveNotification(message);
    }

}