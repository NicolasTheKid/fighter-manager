import { Observer } from '../../notification/observer';
import { Observable } from '../../notification/observable';
import { EventEmitter } from 'events';

export default class CombatNotificationClient extends EventEmitter implements Observer {
    private combatSubject: Observable;

    constructor(combatClient: Observable) {
        super();
        this.combatSubject = combatClient;
    }

    receiveNotification<T>(message: {type: string, data: any}) {
        this.emit('message', message);
    }
}