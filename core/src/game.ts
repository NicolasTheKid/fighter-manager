import { setImmediate, setTimeout } from 'timers';
/* UI */
import GameUI from './ui';
/* Systems */
import { loadMap } from './systems/map';
/* Game specific components */
import { levels } from '../../levels.json';
/* Game Config */
import GameOptions from './gameOptions';
import * as config from '../../config.json';
// import MapComponent from '../ui/components/map.component';

export default class Game {
  /* game loop config */
  private _options: GameOptions;
  /* UI for game */
  private gameUI: GameUI;
  /**
   * Game
   * setup options for game instance
   */
  constructor(GameUI: GameUI) {
    this.SetOptions();
    this.gameUI = GameUI;
  }

  /**
   * Start
   * @description starts the game loop
   */
  public Start(): void {
    /* start up the game loop! */
    /* timestamp of each loop */
    const previousTick = Date.now();
    /* number of times game loop is called */
    const actualTicks = 0;
    /* start the game loop! */
    this.Loop(actualTicks, previousTick);
  }

  public Draw() {
    const levelMap = loadMap(levels[0]);
    // this.gameUI.AddComponent(MapComponent(levelMap));
    this.gameUI.Draw();
  }

  private Loop(actualTicks: number, previousTick: number) {
    const now = Date.now();

    let newActualTicks = actualTicks++;
    let newPreviousTick: number;
    if (previousTick + this._options.tick_length_ms <= now) {
      const delta = (now - previousTick) / 1000;
      newPreviousTick = now;

      /* TODO: update game state */
      this.Update(delta);
      newActualTicks = 0;
    }

    if (Date.now() - previousTick < this._options.tick_length_ms - 16) {
      setTimeout(() => this.Loop(newActualTicks, newPreviousTick), 0);
    } else {
      setImmediate(() => this.Loop(newActualTicks, newPreviousTick), 0);
    }
  }

  private SetOptions() {
    this._options = config;
    this._options.tick_length_ms = this._options.tick_length / this._options.framerate;
  }

  private Update(delta: number) {
    /* update game state */
  }

}