import React, { FunctionComponent, useContext } from 'react';
import { GameContext } from './common/contexts/context';
import { StartScreen } from './ui/screens/start';
import { OptionsMenu } from './ui/components/options/options.menu';

export const Game: FunctionComponent = () => {
  const { state, dispatch } = useContext(GameContext);
  const { uiState } = state;

  return (
    <>
      <StartScreen />
      {uiState.optionMenuDisplayed ? (
        <OptionsMenu top='10%' left='20%' />
      ) : null}
    </>
  );
};