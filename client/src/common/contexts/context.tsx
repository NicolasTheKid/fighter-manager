import React, { createContext, Dispatch, useReducer } from 'react';
import { normalize, join } from 'path';
import { SettingsState, UIState, GameState, settingsStateReducer, uiStateReducer, gameStateReducer } from '../reducers';
import { GameTypes, SettingsStateActions, UIStateActions, GameStateActions } from '../actions';

export const loadOptions = () => {
  let options;
  try {
    const filePath = normalize(join(__dirname, '../../../..', 'settings.json'));
    options = require(filePath);
  } catch (error) {
    options = { width: 800, height: 600 };
  }
  return options;
};

type InitialState= {
  settingsState: SettingsState;
  uiState: UIState;
  gameState: GameState;
};

const initialState: InitialState = {
  settingsState: loadOptions(),
  uiState: { optionMenuDisplayed: false },
  gameState: { status: GameTypes.STOP }
};

const GameContext = createContext<{
  state: InitialState;
  dispatch: Dispatch<SettingsStateActions | UIStateActions | GameStateActions>;
}>({
  state: initialState,
  dispatch: () => null
});

const mainReducer = ({
  settingsState,
  uiState,
  gameState
}: InitialState,
action: SettingsStateActions | UIStateActions | GameStateActions) => ({
  settingsState: settingsStateReducer(settingsState, action as SettingsStateActions),
  uiState: uiStateReducer(uiState, action as UIStateActions),
  gameState: gameStateReducer(gameState, action as GameStateActions)
});

const GameProvider = ({ children }) => {
  const [state, dispatch] = useReducer(mainReducer, initialState);

  return (
    <GameContext.Provider value={{state, dispatch}}>
      {children}
    </GameContext.Provider>
  );
};

export { GameContext, GameProvider };

