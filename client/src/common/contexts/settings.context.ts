export interface ISettingsContext {
  width: number,
  height: number
}