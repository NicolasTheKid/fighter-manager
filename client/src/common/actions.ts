type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined ? {
    type: Key;
  } : {
    type: Key;
    payload: M[Key];
  }
};

export enum SettingsTypes {
  UPDATE,
  RESET
}

export enum UITypes {
  OPTIONS,
}

export enum GameTypes {
  START,
  STOP,
  PAUSE
}

type SettingsPayload = {
  [SettingsTypes.UPDATE]: {
    size: string;
    fullscreen: boolean;
  };
  [SettingsTypes.RESET]: {
    size: string;
    fullscreen: boolean;
  }
};

type UIPayload = {
  [UITypes.OPTIONS]: {
    optionMenuDisplayed: boolean;
  };
};

type GamePayload = {
  [GameTypes.START]: {
    status: GameTypes;
  };
  [GameTypes.STOP]: {
    status: GameTypes;
  };
  [GameTypes.PAUSE]: {
    status: GameTypes;
  };
};

export type SettingsStateActions = ActionMap<SettingsPayload>[keyof ActionMap<SettingsPayload>];
export type UIStateActions = ActionMap<UIPayload>[keyof ActionMap<UIPayload>];
export type GameStateActions = ActionMap<GamePayload>[keyof ActionMap<GamePayload>];