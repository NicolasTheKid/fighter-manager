import { UITypes, GameTypes, SettingsStateActions, UIStateActions, GameStateActions, SettingsTypes } from './actions';

export type UIState = {
  optionMenuDisplayed: boolean;
};

export type SettingsState = {
  size: string;
  fullscreen: boolean;
};

export type GameState = {
  status: GameTypes;
};

export const settingsStateReducer = (state: SettingsState, action: SettingsStateActions | UIStateActions | GameStateActions) => {
  switch (action.type) {
    case SettingsTypes.UPDATE:
      return state = action.payload;
    case SettingsTypes.RESET:
      return action.payload;
    default:
      return state;
  }
};

export const uiStateReducer = (state: UIState, action: SettingsStateActions | UIStateActions | GameStateActions) => {
  switch (action.type) {
    case UITypes.OPTIONS:
      return state = action.payload;
    default:
      return state;
  }
};

export const gameStateReducer = (state: GameState, action: SettingsStateActions | UIStateActions | GameStateActions) => {
  switch (action.type) {
    case GameTypes.START:
    case GameTypes.STOP:
    case GameTypes.PAUSE:
      return state = action.payload;
    default:
      return state;
  }
};