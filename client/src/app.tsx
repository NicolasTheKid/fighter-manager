import React from 'react';
import { render } from 'react-blessed';
import * as blessed from 'blessed';
import { Game } from './game';
import { loadOptions, GameProvider } from './common/contexts/context';

const { width, height } = loadOptions();

const screen = blessed.screen({
  autoPadding: true,
  smartCSR: true,
  title: 'button test',
  width: width,
  height: height
});

screen.key(['Escape', 'q', 'C-c'], () => {
  return process.exit(0);
});

screen.key(['tab'], () => {
  screen.focusNext();
});

render(
  <GameProvider>
    <Game />
  </GameProvider>, screen);