import React, { useContext } from 'react';
import { StartButton } from '../components/start.button';
import { OptionsButton } from '../components/options/options.button';

export const StartScreen = () => {
  return (
    <blessed-box
      top={'center'}
      left={'center'}
      width={'100%'}
      height={'100%'}
      border={{type: 'line'}}
      style={{bg: 'gray'}}>
        <StartButton top="82%" left="center"/>
        <OptionsButton top="90%" left="center"/>
    </blessed-box>
  );
};