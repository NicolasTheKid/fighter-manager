import React, { useContext } from 'react';
import { Menu } from '@fm/ui/dist/menu.component';
import { GameContext } from '../../../common/contexts/context';
import { UITypes, SettingsTypes } from '../../../common/actions';

type OptionsMenuProps = {
  top?: string | number;
  left?: string | number;
  bottom?: string | number;
  right?: string | number;
};

export const OptionsMenu = ({ top, left, bottom, right }: OptionsMenuProps) => {
  const { state, dispatch } = useContext(GameContext);
  const { settingsState, uiState } = state;

  const size = ['small', 'medium', 'large'];

  const toggleOptionsMenu = () => dispatch({ type: UITypes.OPTIONS, payload: { optionMenuDisplayed: !uiState.optionMenuDisplayed } });
  const setResolution = () => {
    const currentIndex = size.indexOf(settingsState.size);
    const newSize = currentIndex >= size.length ? size[0] : size[currentIndex + 1];

    dispatch({
      type: SettingsTypes.UPDATE,
      payload: {
        size: newSize,
        fullscreen: settingsState.fullscreen
      }
    });
  };

  return (
    <Menu
      top={top}
      left={left}
      bottom={bottom}
      right={right}
      width={'60%'}
      height={'60%'}
      closeMenu={toggleOptionsMenu}>
      <blessed-form>
        <blessed-button
          border={{ type: 'line' }}
          top={'20%'}
          left={'center'}
          onPress={setResolution}
          shrink
          keys
          mouse>
          {settingsState.size}
        </blessed-button>
        <blessed-text>Fullscreen</blessed-text>
        <blessed-button
          border={{ type: 'line' }}
          top={'30%'}
          left={'center'}
          shrink
          keys
          mouse>
          {settingsState.fullscreen ? 'on' : 'off'}
        </blessed-button>
      </blessed-form>
    </Menu>
  );
};
