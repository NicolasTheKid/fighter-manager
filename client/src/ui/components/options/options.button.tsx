import React, { useContext } from 'react';
import { GameContext } from '../../../common/contexts/context';
import { UITypes } from '../../../common/actions';

type OptionsButtonProps = {
  top?: string | number;
  left?: string | number;
  bottom?: string | number;
  right?: string | number;
}

export const OptionsButton = ({ top, left, bottom, right }: OptionsButtonProps) => {
  const { state, dispatch } = useContext(GameContext);
  const { uiState } = state;
  const toggleOptionsMenu = () => dispatch({type: UITypes.OPTIONS, payload: { optionMenuDisplayed: !uiState.optionMenuDisplayed }});

  return (
    <blessed-button
      onPress={toggleOptionsMenu}
      border={{ type: 'line' }}
      top={top}
      left={left}
      bottom={bottom}
      right={right}
      style={{
        fg: 'green',
        focus: {
          bg: 'blue'
        },
        hover: {
          bg: 'cyan'
        }
      }}
      shrink
      keys
      mouse>
      Options
    </blessed-button>
  );
};