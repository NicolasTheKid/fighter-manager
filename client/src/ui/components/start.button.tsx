import React, { useContext } from 'react';
import { GameContext } from '../../common/contexts/context';
import { GameTypes } from '../../common/actions';

type StartButtonProps = {
  top?: string | number;
  left?: string | number;
  bottom?: string | number;
  right?: string | number;
};

export const StartButton = ({ top, left, bottom, right }: StartButtonProps) => {
  const { dispatch } = useContext(GameContext);
  const toggleGameState = () => dispatch({type: GameTypes.START, payload: { status: GameTypes.START }});

  return (
    <blessed-button
      top={top}
      left={left}
      bottom={bottom}
      right={right}
      border={{ type: 'line' }}
      style={{
        fg: 'green',
        hover: {
          fg: 'cyan'
        },
        focus: {
          fg: 'blue'
        }
      }}
      onPress={toggleGameState}
      shrink
      mouse
      keys>
      Start
    </blessed-button>
  );
};