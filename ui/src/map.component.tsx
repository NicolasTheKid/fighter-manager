import React, { FunctionComponent } from 'react';

type MapProps = {
  top?: string,
  right?: string,
  bottom?: string,
  left?: string,
  content: string
};

export const MapComponent: FunctionComponent<MapProps> = ({bottom, content, left, right, top}) => (
  <blessed-box 
    border={{type: 'line'}}
    bottom={bottom}
    left={left}
    top={top}
    shrink>
    {content}
  </blessed-box>
);