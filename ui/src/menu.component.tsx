import React, { PropsWithChildren } from 'react';

type MenuProps = {
  top?: string | number;
  left?: string | number;
  bottom?: string | number;
  right?: string | number;
  width: string | number;
  height: string | number;
  closeMenu: () => void
};

export const Menu = ({ top, left, bottom, right, width, height, closeMenu, children }: PropsWithChildren<MenuProps>) => (
  <blessed-box
    border={{ type: 'line' }}
    top={top}
    left={left}
    bottom={bottom}
    right={right}
    width={width}
    height={height}>
    <blessed-button
      left={'1%'}
      onPress={closeMenu}
      mouse
      keys
      shrink>
      x
      </blessed-button>
    <blessed-line orientation="horizontal" top="5%"></blessed-line>
    <blessed-box top="15%">
      {children}
    </blessed-box>
  </blessed-box>
);