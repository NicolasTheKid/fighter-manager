import React, { FunctionComponent } from 'react';
import * as blessed from 'blessed';
import { render } from 'react-blessed';
import { MapComponent } from './map.component';

const App: FunctionComponent<{}> = () => (
  <MapComponent 
    content='test test' 
    top='10%' 
    left='10%'/>
);

const screen = blessed.screen({
  autoPadding: true,
  smartCSR: true,
  title: 'react-blessed testing'
});

screen.key(['q', 'C-c'], () => {
  return process.exit(0);
});

render(<App/>, screen);