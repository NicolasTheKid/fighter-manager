# Fighter Manager (working title)

## What's it about

Fighter manager is a management sim where you take care of a team of arena fighters (think gladiators).

## Get it running

### Dependencies

This project is currently setup with yarn workspaces, so it requires yarn to be installed globally.

### Spin up

To get the current UI up, run the following commands in the given order

```shell
# first we need to install npm packages
$ yarn
# then we need to build the UI component library
$ yarn workspace @fm/ui build
# finally, we need to build the "UI client"
$ yarn workspace @fm/client build
# now we can spin up the UI
$ yarn workspace @fm/client start
```

To close the game simply press `q` or `Control+c`.

## Current Task

- Get UI components setup
- Setup up UI screens for testing systems
- Determine Battle Loop

## Pending tasks

- [x] Fighter/NPC generation
  - [x] Balance out Fighter stats
  - [x] Figure out Fighter/NPC equipment
  - [x] Figure out Fighter statuses
  - ~~Fighter moods?~~
- [ ] Battle system
  - [ ] Figure out battle system basics
  - [ ] How do we handle different types of battles
    - [ ] Single battle
    - [ ] 2 v 2
    - [ ] Team battle
    - [ ] PvE
- [ ] Market/shop system
  - [ ] monetary system
  - [ ] items!
  - [ ] item categories and rarities
- [ ] Game loop
  - [ ] UI layer
  - [ ] System communication
    - [x] basic nofitication system built